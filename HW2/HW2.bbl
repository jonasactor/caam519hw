\begin{thebibliography}{1}

\bibitem{DLMF}
Nist digital library of mathematical functions.
\newblock \url{dlmf.nist.gov/19.12}, Release 1.0.12 of 2016-09-09.
\newblock F.~W.~J. Olver, A.~B. {Olde Daalhius}, D.~W. Lozier, B.~I. Schneider,
  R.~F. Boisvert, C.~W. Clark, B.~R. Miller and B.~V. Saunders, eds.

\bibitem{ferrer}
Esteban Ferrer.
\newblock {\em A high order Discontinuous Galerkin- Fourier incompressible 3D
  Navier-Stokes solver with rotating sliding meshes for simulating cross-flow
  turbines}.
\newblock PhD thesis, Brasenose College, Department of Engineering Science,
  University of Oxford, 2012.
\newblock [Online; accessed 15-Sept-2016].
  \url{www.eng.ox.ac.uk/civil/publications/theses/ferrer.pdf}.

\bibitem{Kolmogorov}
A.~N. Kolmogorov.
\newblock On the representation of continuous functions of many variables by
  superpositions of continuous functions of one variable and addition.
\newblock {\em Doklay Akademii Nauk USSR}, 14(5):953--956, 1957.

\bibitem{OEIS}
Paolo~P. Lava.
\newblock A273800.
\newblock \url{oeis.org/A273800}, 2016.
\newblock [Online; accessed 15-Sept-2016].

\bibitem{stackoverflowCppRelocation}
StackOverflow.
\newblock ``relocation r\_x86\_64\_32s against `` linking error.
\newblock \url{https://stackoverflow.com/q/19768267}, 4-Nov-2013.
\newblock [Online; accessed 14-Sept-2016].

\bibitem{stackoverflowCppFpic}
StackOverflow.
\newblock What does -fpic mean when building a shared library?
\newblock \url{https://stackoverflowcom/q/966960}, 8-June-2009.
\newblock [Online; accessed 14-Sept-2016].

\bibitem{WikiPython}
Wikipedia.
\newblock Python (programming language).
\newblock \url{https://en.wikipedia.org/wiki/Python_(programming_language)}.
\newblock [Online; accessed 14-Sept-2016].

\end{thebibliography}
