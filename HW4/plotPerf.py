#! /usr/bin/env python
import os
import numpy as np
from matplotlib.pyplot import legend, loglog, show, title, xlabel, ylabel, savefig, clf


lambda_par = [1.0, 2.0, 5.0]
m = [1, 2]
sol = ['lu','gamg']
num_ref = 4

for ms in range(len(m)):
	for k in range(len(lambda_par)):
		flops_LU = []
		flops_GAMG = []
		for st in range(len(sol)):
			N = []
			err_2 = []
			err_inf = []
			for i in range(num_ref):
				modstr = ['perf',str(i),'lambda',str(k),'solver',sol[st],'m',str(m[ms])]
				modname = '-'.join(modstr)
				options = ['-par', str(lambda_par[k]),'-pc_type',sol[st],'-da_grid_x','17','-da_grid_y','17','-da_refine',str(i),'-mms',str(m[ms]),'-log_view',':%s.py:ascii_info_detail' % modname]
				syscall = './ex5 '+' '.join(options)
				output = os.popen(syscall).read()
				output_list = output.split('_')
				N.append(float(output_list[0]))
				err_2.append(float(output_list[1]))
				err_inf.append(float(output_list[2]))
				perfmod = __import__(modname)
				if st == 0:
					flops_LU.append(perfmod.Stages['Main Stage']['summary'][0]['flops'])		
				if st == 1:
					flops_GAMG.append(perfmod.Stages['Main Stage']['summary'][0]['flops'])		
		NN = np.array(N)
		e2 = np.array(err_2)
		ei = np.array(err_inf)
		if m[ms] == 1:
			loglog(NN,e2,'r',NN, NN** -1.5, 'r--',NN,ei,'g',NN,1/NN,'g--')
			title(r'SNES-%s Lane-Emden MMS u = x(1-x)y(1-y)' %sol[st])
			xlabel('Number of DoF N')
			ylabel('Solution Error e')
			legend(['$l_2$', '$h^{-3}= N^{-3/2}$', '$l_\infty$', '$h^{-2} = N^{-1}$'], 'upper right')
			fig_name = 'SNES-'+sol[st]+'-lambda-'+str(lambda_par[k])+'-convergence-m1.pdf'
			savefig(fig_name)
			clf()
		if m[ms] == 2:
			loglog(NN,e2,'r',NN, NN ** -1.5, 'r--',NN,ei,'g',NN,1/NN,'g--')
			title_name = 'SNES-'+sol[st]+' Lane-Emden MMS $u= \sin(\pi x) \sin(\pi y)$'
			title(title_name)
			xlabel('Number of DoF N')
			ylabel('Solution Error e')
			legend(['$\ell_2$', '$h^{-3} = N^{-3/2}$', '$\ell_\infty$', '$h^{-2} = N^{-1}$'], 'upper right')
			fig_name = 'SNES-'+sol[st]+'-lambda-'+str(lambda_par[k])+'-convergence-m2.pdf'
			savefig(fig_name)
			clf()
			loglog(flops_LU,e2,'r',flops_GAMG,e2,'g')
			title('SNES Lane-Emden MMS Work-Precision')
			xlabel('Work (flops)')
			ylabel('Solution Error $e$')
			legend(['$\ell_2$ vs. LU','$\ell_2$ vs. GAMG'], 'upper right')
			fig_name = 'Work-Precision-SNES-lambda-'+str(lambda_par[k])+'-workprec.pdf'
			savefig(fig_name)
			clf()
			
os.system('rm -f perf*.pyc*')
os.system('rm -f perf*.py')
os.system('rm -f -r graphs')
os.system('mkdir graphs')	
os.system('mv *.pdf graphs/')
