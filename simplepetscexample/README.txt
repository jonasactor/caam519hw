Jonas Actor
CAAM 519
HW 1

1.1 PETSc is installed.
1.2 simplepetscexample is cloned from bitbucket
1.3 Repository created
    PDF of LaTeX code is in PII-1Latex/Problem\ II-1.pdf
1.4 simplepetscexample/makefile
1.5 simplepetscexample/bin/plotPerf.py for script
    simplepetscexample/SNES-ILU-GAMG.png for plot of preconditioned linear solvers
    simplepetscexample/SNES-ILU-GAMG-log.png for log-plot of same data
1.6 simplepetscexample/bin/plotPerf.py for script (same script as 1.5)
    simplepetscexample/JAC-ILU-GAMG.png for plot of Jacobian computation time
    simplepetscexample/JAC-ILU-GAMG-log. png for log-plot of same data
    simplepetscexample/Res-ILU-GAMG.png for plot of Residual computation time
    simplepetscexample/Res-ILU-GAMG-log.pny for log-plot of same data
