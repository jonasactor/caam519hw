#! /usr/bin/env python
import os

sizes = []
timesSNES = []
timesILU = []
timesGAMG = []
timesILU_JAC = []
timesILU_Res = []
timesGAMG_JAC = []
timesGAMG_Res = []

for k in range(5):
  Nx = 10 * 2**k
  modname = 'perf%d' % k
  options = ['-da_grid_x', str(Nx), '-da_grid_y', str(Nx), '-log_view', ':%s.py:ascii_info_detail' % modname]
  os.system('./bin/ex5 '+' '.join(options))
  os.system('mv %s.py bin' % modname)
  perfmod = __import__(modname)
  sizes.append(Nx**2)
  timesSNES.append(perfmod.Stages['Main Stage']['SNESSolve'][0]['time'])

  modname2 = 'perf2-%d' % k
  options2 = ['-da_grid_x', str(Nx), '-da_grid_y', str(Nx), '-pc_type', 'ilu', '-log_view', ':%s.py:ascii_info_detail' % modname2] 
  os.system('./bin/ex5 '+' '.join(options2))
  os.system('mv %s.py bin' % modname2)
  perfmod2 = __import__(modname2)
  timesILU.append(perfmod2.Stages['Main Stage']['KSPSolve'][0]['time'])
  timesILU_JAC.append(perfmod2.Stages['Main Stage']['SNESJacobianEval'][0]['time'])
  timesILU_Res.append(perfmod2.Stages['Main Stage']['SNESFunctionEval'][0]['time'])

  modname3 = 'perf3-%d' % k
  options3 = ['-da_grid_x', str(Nx), '-da_grid_y', str(Nx), '-pc_type', 'gamg', '-log_view', ':%s.py:ascii_info_detail' % modname3]
  os.system('./bin/ex5 '+' '.join(options3))
  os.system('mv %s.py bin' % modname3)
  perfmod3 = __import__(modname3)
  timesGAMG.append(perfmod3.Stages['Main Stage']['KSPSolve'][0]['time'])
  timesGAMG_JAC.append(perfmod3.Stages['Main Stage']['SNESJacobianEval'][0]['time'])
  timesGAMG_Res.append(perfmod3.Stages['Main Stage']['SNESFunctionEval'][0]['time'])

print zip(sizes, timesSNES, timesILU, timesGAMG)

from pylab import legend, plot, loglog, show, title, xlabel, ylabel, savefig, legend
plot(sizes, timesSNES)
title('SNES ex5')
xlabel('Problem Size $N$')
ylabel('Time (s)')
show()
savefig('SNES.png')

loglog(sizes, timesSNES)
title('SNES ex5')
xlabel('Problem Size $N$')
ylabel('Time (s)')
show()
savefig('SNES-log.png')

plot(sizes, timesSNES, label='SNES')
plot(sizes, timesILU, label='ILU')
plot(sizes, timesGAMG, label='GAMG')
title('ex5 SNES, ILU, GAMG')
xlabel('Problem Size $N$')
ylabel('Time (s)')
legend(loc='upper left')
show()
savefig('SNES-ILU-GAMG.png')

loglog(sizes, timesSNES, label='SNES')
loglog(sizes, timesILU, label='ILU')
loglog(sizes, timesGAMG, label='GAMG')
title('ex5 SNES, ILU, GAMG')
xlabel('Problem Size $N$')
ylabel('Time (s)')
legend(loc='upper left')
show()
savefig('SNES-ILU-GAMG-log.png')

plot(sizes, timesILU_JAC, label='ILU')
plot(sizes, timesGAMG_JAC, label='GAMG')
title('ex5 Jacobian ILU, GAMG')
xlabel('Problem Size $N$')
ylabel('Time (s)')
legend(loc='upper left')
show()
savefig('JAC-ILU-GAMG.png')

loglog(sizes, timesILU_JAC, label='ILU')
loglog(sizes, timesGAMG_JAC, label='GAMG')
title('ex5 SNES, ILU, GAMG')
xlabel('Problem Size $N$')
ylabel('Time (s)')
legend(loc='upper left')
show()
savefig('JAC-ILU-GAMG-log.png')

plot(sizes, timesILU_Res, label='ILU')
plot(sizes, timesGAMG_Res, label='GAMG')
title('ex5 Residual ILU, GAMG')
xlabel('Problem Size $N$')
ylabel('Time (s)')
legend(loc='upper left')
show()
savefig('Res-ILU-GAMG.png')

loglog(sizes, timesILU_Res, label='ILU')
loglog(sizes, timesGAMG_Res, label='GAMG')
title('ex5 Residual ILU, GAMG')
xlabel('Problem Size $N$')
ylabel('Time (s)')
legend(loc='upper left')
show()
savefig('Res-ILU-GAMG-log.png')

