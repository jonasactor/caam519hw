// Jonas Actor
// CAAM 519 HW 3
// QR Decomposition 

#include <petscmat.h>
#include <time.h>
#include <stdlib.h>

static char help[] = "QR Factorization.\n";

int OurQR(Mat *A, PetscInt m, PetscInt n, Mat *Q, Mat *R, PetscReal *e_inf, PetscReal *e_1, PetscReal *e_f)
{
Mat		Result;
Vec		q_j,q_k,a_k;
PetscErrorCode	ierr;
PetscInt	j, k, *r_col, *q_col;
PetscScalar	*q_k_sc, neg_one = -1.0;
PetscReal	*r;

ierr = VecCreate(PETSC_COMM_WORLD,&q_j); CHKERRQ(ierr);
ierr = VecSetSizes(q_j,PETSC_DECIDE,m); CHKERRQ(ierr);
ierr = VecSetFromOptions(q_j);CHKERRQ(ierr);

ierr = VecDuplicate(q_j,&q_k);CHKERRQ(ierr);
ierr = VecDuplicate(q_j,&a_k);CHKERRQ(ierr);

ierr = PetscMalloc1(m,&r);CHKERRQ(ierr);
ierr = PetscMalloc1(m,&q_k_sc);CHKERRQ(ierr);
ierr = PetscMalloc1(n,&r_col);CHKERRQ(ierr);
ierr = PetscMalloc1(m,&q_col);CHKERRQ(ierr);

for (j=0;j<n;j++) {
	r_col[j] = j;
}
for (j=0;j<m;j++) {
	q_col[j] = j;
}

ierr = MatCreate(PETSC_COMM_WORLD,&Result);CHKERRQ(ierr);
ierr = MatSetSizes(Result,PETSC_DETERMINE,PETSC_DETERMINE,m,n);CHKERRQ(ierr);
ierr = MatSetFromOptions(Result);CHKERRQ(ierr);
ierr = MatSetUp(Result);CHKERRQ(ierr);

/*__________________________________________________
 * QR Implementation
 * _________________________________________________*/

ierr = MatZeroEntries(*Q);CHKERRQ(ierr);
ierr = MatZeroEntries(*R);CHKERRQ(ierr);

for (k=0; k<n; k++) {
	for (j=0;j<n;j++){
		r[j] = 0.0;
	}
	ierr = MatGetColumnVector(*A,a_k,k);CHKERRQ(ierr);
	ierr = VecCopy(a_k,q_k);CHKERRQ(ierr);
	for (j=0; j<k; j++) {
		ierr = MatGetColumnVector(*Q,q_j,j);CHKERRQ(ierr);
		ierr = VecDot(q_j,a_k,&r[j]);CHKERRQ(ierr);
		ierr = VecAXPY(q_k,neg_one*r[j],q_j);CHKERRQ(ierr);
	}
	ierr = VecNorm(q_k, NORM_2, &(r[k]));CHKERRQ(ierr);
	ierr = MatSetValues(*R,n,r_col,1,&k,r,INSERT_VALUES);CHKERRQ(ierr);
	ierr = MatAssemblyBegin(*R,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(*R,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr = VecScale(q_k,(1.0/r[k]));CHKERRQ(ierr);
	ierr = VecGetArray(q_k,&q_k_sc);CHKERRQ(ierr);
	ierr = MatSetValues(*Q,m,q_col,1,&k,q_k_sc,INSERT_VALUES);CHKERRQ(ierr);
	ierr = VecRestoreArray(q_k,&q_k_sc);CHKERRQ(ierr);
	ierr = MatAssemblyBegin(*Q,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(*Q,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
}

/*__________________________________________________
 * Check Solution
 * _________________________________________________*/

ierr = MatMatMult(*Q,*R,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&Result);CHKERRQ(ierr);
ierr = MatAXPY(Result,neg_one,*A,SAME_NONZERO_PATTERN);CHKERRQ(ierr);
ierr = MatNorm(Result,NORM_INFINITY,e_inf);CHKERRQ(ierr);
ierr = MatNorm(Result,NORM_1,e_1);CHKERRQ(ierr);
ierr = MatNorm(Result,NORM_FROBENIUS,e_f);CHKERRQ(ierr);

/*__________________________________________________
 * Destroy and Finalize when done
 * _________________________________________________*/

ierr = PetscFree(r_col);CHKERRQ(ierr);
ierr = PetscFree(q_col);CHKERRQ(ierr);
ierr = PetscFree(r);CHKERRQ(ierr);
ierr = PetscFree(q_k_sc);CHKERRQ(ierr);
ierr = VecDestroy(&a_k);CHKERRQ(ierr);
ierr = VecDestroy(&q_j);CHKERRQ(ierr);
ierr = VecDestroy(&q_k);CHKERRQ(ierr);
ierr = MatDestroy(&Result);CHKERRQ(ierr);
return 0;
}


int main(int argc,char **args)
{
Mat		A,Q,R;
PetscInt 	m=4,n=3,j, *allrows, *allcols, output=0;
PetscScalar	*a;
PetscReal 	e_inf, e_1, e_f;
PetscErrorCode 	ierr;
//PetscRandom	rctx;

ierr = PetscInitialize(&argc,&args,(char*)0,help);CHKERRQ(ierr);
ierr = PetscOptionsGetInt(NULL,NULL,"-m",&m,NULL);CHKERRQ(ierr);
ierr = PetscOptionsGetInt(NULL,NULL,"-n",&n,NULL);CHKERRQ(ierr);
ierr = PetscOptionsGetInt(NULL,NULL,"-o",&output,NULL);CHKERRQ(ierr);

ierr = MatCreate(PETSC_COMM_WORLD,&A);CHKERRQ(ierr);
ierr = MatSetSizes(A,PETSC_DECIDE,PETSC_DECIDE,m,n);CHKERRQ(ierr);
ierr = MatSetFromOptions(A);CHKERRQ(ierr);
ierr = MatSetUp(A);CHKERRQ(ierr);

ierr = MatCreate(PETSC_COMM_WORLD,&Q);CHKERRQ(ierr);
ierr = MatSetSizes(Q,PETSC_DECIDE,PETSC_DECIDE,m,n);CHKERRQ(ierr);
ierr = MatSetFromOptions(Q);CHKERRQ(ierr);
ierr = MatSetUp(Q);CHKERRQ(ierr);

ierr = MatCreate(PETSC_COMM_WORLD,&R);CHKERRQ(ierr);
ierr = MatSetSizes(R,PETSC_DECIDE,PETSC_DECIDE,n,n);CHKERRQ(ierr);
ierr = MatSetFromOptions(R);CHKERRQ(ierr);
ierr = MatSetUp(R);CHKERRQ(ierr);

ierr = PetscMalloc1(n,&allcols);CHKERRQ(ierr);
for(j=0;j<n;j++){
	allcols[j] = j;
}
ierr = PetscMalloc1(m,&allrows);CHKERRQ(ierr);
for(j=0;j<m;j++){
	allrows[j] = j;
}
ierr = PetscMalloc1(m*n,&a);CHKERRQ(ierr);
srand( time(NULL));
for (j=0;j<m*n;j++){
	a[j] = rand()%(m*m*n*n);
}
ierr = MatSetValues(A,m,allrows,n,allcols,a,INSERT_VALUES);CHKERRQ(ierr);
ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

/*__________________________________________________
 * Call QR Factorization
 * _________________________________________________*/

OurQR(&A,m,n,&Q,&R,&e_inf,&e_1,&e_f);

/*__________________________________________________
 * Print Results to Screen
 * _________________________________________________*/

ierr = MatAssemblyBegin(Q,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
ierr = MatAssemblyEnd(Q,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
ierr = MatAssemblyBegin(R,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
ierr = MatAssemblyEnd(R,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

if (output) {
	ierr = PetscPrintf(PETSC_COMM_WORLD,"A:\n");CHKERRQ(ierr);
	ierr = MatView(A,PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"\nQ:\n");CHKERRQ(ierr);
	ierr = MatView(Q,PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"\nR:\n");CHKERRQ(ierr);
	ierr = MatView(R,PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr);
}
ierr = PetscPrintf(PETSC_COMM_WORLD,"\n          1-Norm Error:\t%6.4e\n",(double)e_1);CHKERRQ(ierr);
ierr = PetscPrintf(PETSC_COMM_WORLD,"  Frobenius-Norm Error:\t%6.4e\n",(double)e_f);CHKERRQ(ierr);
ierr = PetscPrintf(PETSC_COMM_WORLD,"        Inf-Norm Error:\t%6.4e\n\n",(double)e_inf);CHKERRQ(ierr);

ierr = PetscFree(a);CHKERRQ(ierr);
ierr = PetscFree(allrows);CHKERRQ(ierr);
ierr = PetscFree(allcols);CHKERRQ(ierr);
ierr = MatDestroy(&A);CHKERRQ(ierr);
ierr = MatDestroy(&Q);CHKERRQ(ierr);
ierr = MatDestroy(&R);CHKERRQ(ierr);
ierr = PetscFinalize();
return 0;
}
